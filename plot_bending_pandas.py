import sys
import matplotlib.pyplot as plt
import pandas as pd

file_name = sys.argv[1]

col_names=["Time sec","Extension mm","Load N","Stress MPa","Cycle Count ","Total Cycle Count ","Repetitions Count ","Strain [Exten.] %","Tenacity gf/tex"]


df = pd.read_csv(file_name, delimiter=",", names=col_names)
df = df.dropna()
df = df[df["Time sec"].apply(lambda x: x.isnumeric())]
print(df.tail())
print(df.head())
df.plot(x="Stress MPa", y="Strain [Exten.] %")
plt.show()
