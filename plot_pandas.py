import sys
import matplotlib.pyplot as plt
import pandas as pd

file_name = sys.argv[1]

df = pd.read_csv(file_name, delimiter="\t", header=None)

# Since the file doesn't have cols labled, we need to add some
col_names = ["Wavelenght (nm)", "Intensity (a.u.)"]
df.rename(columns={0: col_names[0], 1: col_names[1]}, inplace=True)

# pandas has a plotting function
df.plot(x=col_names[0], y=col_names[1])
plt.show()

df.to_excel("pandas.xlsx")
