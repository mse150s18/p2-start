# Project 2

This repository has some template data files and starter code for our Spring 2018 project #2.

The current example code can be invoked with:

```
$ python plot.py spectra/Sp15_245L_sect-001_group-2-4_spectrum-H2
```

or

```
$ python plot_pandas.py spectra/Sp15_245L_sect-001_group-2-4_spectrum-H2
```

Both scripts "do" the same thing, but both use different libraries.
You can use either scripts as a starting point if your group is stuck on project 2.
You can also check out [last years project 2 submissions.](https://bitbucket.org/account/user/mse150s17/projects/P2)
