# This example uses numpy, matplotlib, and xlsxwriter

import matplotlib.pyplot as plt
import xlsxwriter
import numpy as np
import sys

file_name = sys.argv[1]
data = np.genfromtxt(file_name, delimiter="\t")

x, y = data[:, 0], data[:, 1]

plt.plot(x, y)
plt.show()

# Create excell file
workbook = xlsxwriter.Workbook('spectra_data.xlsx')
# Add a worksheet to our workbook
worksheet = workbook.add_worksheet()

# On the ith row, write x values in col 0, y values in col 1
for i, (x, y) in enumerate(zip(x, y)):
    worksheet.write(i, 0, x)
    worksheet.write(i, 1, y)

# Close and save the workbook
workbook.close()
